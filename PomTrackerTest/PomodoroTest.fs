﻿module PomodoroTest

open System
open Fuchu
open Foq
open FsUnit

open PomodoroTracker.Persistence

let enumerator (en: Collections.Generic.IEnumerator<'a>) =
    fun () ->
        match en.MoveNext() with
        | true -> en.Current
        | false -> failwith "Out of sequence"

let incrNow (init:DateTime) (incr: TimeSpan) =
    let now = Seq.initInfinite (fun _ -> incr) |> Seq.scan (fun cur i -> cur + i) init
    in enumerator <| now.GetEnumerator()

let minute = TimeSpan.FromMinutes << float

[<Tests>]
let tests =
    testList "Pomodoro engine test" [
        testList "Incremental Now" [
            test "Increase Now a minute every call" {
                let nowf = incrNow (new DateTime(2014,1,1, 13,0,0)) (minute 1)
                nowf() |> should equal (new DateTime(2014,1,1, 13,0,0))
                nowf() |> should equal (new DateTime(2014,1,1, 13,1,0))
                nowf() |> should equal (new DateTime(2014,1,1, 13,2,0))
            }
        ]

        test "Pomodoro type is initialized with no saved time" {
            let now = new DateTime(2014, 1, 1)
            let storage = Mock<IPomodoroStorage>()
                            .Setup(fun o -> <@ o.LoadTrackedTime now @>).Returns(Choice1Of2 [])
                            .Create()
            let pom = Pomodoro.create(storage, now)
            pom.Context.TotalWork |> should equal TimeSpan.Zero
            pom.Context.TotalBreak |> should equal TimeSpan.Zero
        }

        test "Pomodoro type is initialized with previous saved times" {
            let now = new DateTime(2014, 1, 1)
            let saved = [ {Kind=WorkKind.Work; Begin=new DateTime(2014,1,1, 13,0,0); End=new DateTime(2014,1,1, 13,30,0)} 
                          {Kind=WorkKind.Break; Begin=new DateTime(2014,1,1, 13,30,0); End=new DateTime(2014,1,1, 13,35,0)}
                          {Kind=WorkKind.Work; Begin=new DateTime(2014,1,1, 13,35,0); End=new DateTime(2014,1,1, 14,5,0)}
                          {Kind=WorkKind.Break; Begin=new DateTime(2014,1,1, 14,5,0); End=new DateTime(2014,1,1, 14,10,0)}
                        ]
            let storage = Mock<IPomodoroStorage>()
                            .Setup(fun o -> <@ o.LoadTrackedTime now @>).Returns(Choice1Of2 saved)
                            .Create()
            let pom = Pomodoro.create(storage, now)
            pom.Context.TotalWork |> should equal (TimeSpan.FromHours 1.0)
            pom.Context.TotalBreak |> should equal (TimeSpan.FromMinutes 10.0)
        }

        test "After stop working, work time record must be saved" {
            let now = new DateTime(2014, 1, 1)
            let storage = Mock<IPomodoroStorage>()
                            .Setup(fun o -> <@ o.LoadTrackedTime now @>).Returns(Choice1Of2 [])
                            .Setup(fun o -> <@ o.NewTrackedTime (any()) @>).Returns(Choice1Of2 ())
                            .Create()
            let nowf = incrNow now (minute 1)
            let pom = Pomodoro.create(storage, nowf())
            Pomodoro.work nowf pom
            Pomodoro.stop pom

            Mock.Verify( <@ storage.NewTrackedTime {Kind=WorkKind.Work; Begin=now+(minute 1); End=now+(minute 1)} @>, Times.Once )
        }

        test "Switching to break during working, the work record must be saved" {
            let now = new DateTime(2014, 1, 1)
            let nowf() = now
            let storage = Mock<IPomodoroStorage>()
                            .Setup(fun o -> <@ o.LoadTrackedTime now @>).Returns(Choice1Of2 [])
                            .Setup(fun o -> <@ o.NewTrackedTime (any()) @>).Returns(Choice1Of2 ())
                            .Create()
            let pom = Pomodoro.create(storage, now)
            Pomodoro.work nowf pom
            Pomodoro.takeBreak nowf (TimeSpan.FromMinutes(1.)) pom

            Mock.Verify( <@ storage.NewTrackedTime {Kind=WorkKind.Work; Begin=now; End=now} @>, Times.Once )
        }

        test "After stop break, break time record must be saved" {
            let now = new DateTime(2014, 1, 1)
            let storage = Mock<IPomodoroStorage>()
                            .Setup(fun o -> <@ o.LoadTrackedTime now @>).Returns(Choice1Of2 [])
                            .Setup(fun o -> <@ o.NewTrackedTime (any()) @>).Returns(Choice1Of2 ())
                            .Create()
            let nowf = incrNow now (minute 1)
            let pom = Pomodoro.create(storage, nowf())
            Pomodoro.takeBreak nowf (minute 5) pom
            Pomodoro.stop pom

            Mock.Verify( <@ storage.NewTrackedTime {Kind=WorkKind.Break; Begin=now+(minute 1); End=now+(minute 1)} @>, Times.Once )
        }
    ]