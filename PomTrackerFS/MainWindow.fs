﻿module UI

open System
open System.Windows
open RZ.Wpf
open PomodoroTracker.Persistence

let defaultNow() = DateTime.Now

type MainWindow() as me =
    inherit Window()
    
    do ignore <| XamlLoader.loadFromResource("PomTrackerFs.g.resources", "mainwindow.xaml") (Some me)

    let scope = me.FindName("scope") :?> PomTracker.MainWindowScope
    let controller = Pomodoro.create( new DummyStorage(), DateTime.Now)

    let updateTime() =
        match Pomodoro.state controller with
        | Pomodoro.Break data -> scope.Timer <- data.End - data.Elapsed - data.Start
                                 scope.TotalBreaks <- Pomodoro.getTotalBreak controller + data.Elapsed
        | Pomodoro.Work data -> scope.Timer <- data.End - data.Elapsed - data.Start
                                scope.TotalWorks <- Pomodoro.getTotalWork controller + data.Elapsed
        | _ -> ()

    do controller.TimeChanged 
    |> Observable.subscribe (fun _ -> updateTime()) 
    |> ignore

    member private me.doWork (sender:obj, args: RoutedEventArgs) =
        controller |> Pomodoro.work defaultNow
        scope.Started <- true
        scope.WorkingState <- "WORK"

    member private me.doBreak (sender:obj, args: RoutedEventArgs) =
        controller |> (Pomodoro.takeBreak defaultNow << TimeSpan.FromMinutes << float) scope.BreakTime
        scope.Started <- true
        scope.WorkingState <- "BREAK"

    member private me.doStop (sender:obj, args:RoutedEventArgs) =
        controller |> Pomodoro.stop
        scope.Started <- false
        scope.Timer <- Pomodoro.DefaultWorkTime
        scope.WorkingState <- "IDLE"
