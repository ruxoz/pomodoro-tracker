﻿module Pomodoro

open System
open System.IO
open RZ.NetWrapper
open RZ.Wpf
open PomodoroTracker.Persistence

type StateData = { Start: DateTime; End: DateTime; Elapsed: TimeSpan; Notified: bool }
type WorkState = Idle
               | Work of StateData
               | Break of StateData

let DefaultWorkTime = TimeSpan.FromMinutes 25.0

[<Literal>]
let NormalBreakTime = 5  // minutes

let _shouldNotify now (d:StateData) = (d.End - now <= TimeSpan.Zero) && not d.Notified

[<NoComparison>]
type TContext = { State: WorkState; TotalWork: TimeSpan; TotalBreak: TimeSpan; Timer: IDisposable option; Storage: IPomodoroStorage }
type T(storage: IPomodoroStorage, now: DateTime) =
    let initContext() =
        match storage.LoadTrackedTime now with
        | Choice2Of2 _ -> { State=Idle; TotalWork=TimeSpan.Zero; TotalBreak=TimeSpan.Zero; Timer=None; Storage=storage }
        | Choice1Of2 timeTracks -> { State=Idle
                                     TotalWork=totalTime1 WorkKind.Work timeTracks
                                     TotalBreak=totalTime1 WorkKind.Break timeTracks
                                     Timer=None; Storage=storage
                                   }
    let mutable context = initContext()
    let timeChanged = Event<EventArgs>()
    let soundPlayer = lazy(match Sound.createRandomizer <| DirectoryInfo "audio" with
                           | Choice1Of2 result -> Some result
                           | Choice2Of2 _ -> None)

    interface IDisposable with
        member me.Dispose() = me.Context.Timer |> Option.tryWith dispose
                              me.setContext {me.Context with Timer=None}
    member me.Context with get() = context
    member me.setContext v = context <- v; timeChanged.Trigger EventArgs.Empty
    member me.TimeChanged with get() = timeChanged.Publish

    member me.SoundPlayer with get() = soundPlayer.Force()
    
let create storage = new T(storage)
let _context pom = (pom:T).Context

let getTotalWork pom = (_context pom).TotalWork
let getTotalBreak pom = (_context pom).TotalBreak

let state pom = (_context pom).State
let _tick nowf pom = 
    let now = nowf()
    let updateContext toStateFunc data ctx =
        let elapsed = now - data.Start
        let notified = match _shouldNotify now data with
                       | true -> (pom:T).SoundPlayer |> Option.tryWith Sound.play; true
                       | false -> data.Notified
        {ctx with State=toStateFunc {data with Elapsed=elapsed; Notified=notified}}

    match state pom with
    | Work data -> pom.setContext <| updateContext Work data pom.Context
    | Break data -> pom.setContext <| updateContext Break data pom.Context
    | _ -> failwith "Something's wrong"

let _startTimer nowf toState pom =
    match state pom with
    | Idle ->
        let timer = DispatcherTimer.startTimer (TimeSpan.FromSeconds 0.1) (fun _ -> _tick nowf pom)
        { pom.Context with State=toState; Timer=Some timer}
    | _ -> pom.Context
    |> pom.setContext

let stop pom =
    let saveTrackedTime kind pom data =
        let ctx = _context pom
        match ctx.Storage.NewTrackedTime {Kind=kind; Begin=data.Start; End=data.Start+data.Elapsed} with
        | Choice1Of2 _ -> {pom.Context with State=Idle; TotalBreak=ctx.TotalBreak+data.Elapsed; Timer=None}
        | Choice2Of2 e -> failwithf "Data storing failed: %s" e

    let ctx = _context pom
    ctx.Timer |> Option.tryWith dispose
    match state pom with
    | Break data -> saveTrackedTime WorkKind.Break pom data
    | Work data -> saveTrackedTime WorkKind.Work pom data
    | Idle -> pom.Context
    |> pom.setContext

let work nowf pom =
    let now = nowf()
    stop pom

    pom |> _startTimer nowf (Work { Start=now
                                    End=now+DefaultWorkTime
                                    Elapsed=TimeSpan.Zero
                                    Notified=false })

let takeBreak nowf breakTime pom =
    let now = nowf()
    stop pom

    pom |> _startTimer nowf (Break { Start=now
                                     End=now+breakTime
                                     Elapsed=TimeSpan.Zero
                                     Notified=false})

