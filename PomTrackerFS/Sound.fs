﻿module Sound

open System
open System.Media

open RZ.NetWrapper

let randomArrayIndex = Random.next +=> Array.length
let randomArray random lst = Array.get lst (randomArrayIndex random lst)

let playSound filename =
    use player = new SoundPlayer(filename:string)
    player.Play()

type Randomizer(waveFiles, random) =
    static member create random files = Randomizer(files, random)
    member self.Play() = waveFiles |> randomArray random |> playSound

let play randomizer = (randomizer:Randomizer).Play()

let createRandomizer audioDirectory =
    try
        audioDirectory
        |> DirInfo.getFiles "*.wav" 
        |> Array.map (fun dir -> dir.FullName)
        |> Randomizer.create (Random())
        |> Choice1Of2
    with
    | :? IO.DirectoryNotFoundException -> Choice2Of2 <| sprintf "Invalid directory %s" audioDirectory.Name