﻿module MainApp

open System
open System.Windows

// Application Entry point
[<STAThread>]
[<EntryPoint>]
let main(_) = (new Application()).Run(UI.MainWindow())