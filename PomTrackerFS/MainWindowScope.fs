﻿namespace PomTracker

open System

type MainWindowScope() =
    inherit RZ.Wpf.ViewModelBase()

    let mutable totalBreaks = TimeSpan.Zero
    let mutable totalWorks = TimeSpan.Zero
    let mutable timer = TimeSpan.Zero
    let mutable breakTime = 0
    let mutable started = false
    let mutable workingState = String.Empty

    member me.TotalBreaks with get ()= totalBreaks
                          and  set v = me.setValue( &totalBreaks, v, "TotalBreaks")

    member self.TotalWorks with get () = totalWorks
                           and  set value = self.setValue( &totalWorks, value, "TotalWorks")

    member self.Timer with get () = timer
                      and  set value = self.setValue( &timer, value, "Timer")
    member self.BreakTime   with get () = breakTime
                            and  set value = self.setValue( &breakTime, value, "BreakTime")
    member self.Started     with get () = started
                            and  set value = self.setValue( &started, value, "Started")
    member self.IsOverTime  = timer < TimeSpan.Zero

    member self.WorkingState    with get () = workingState
                                and  set value = self.setValue( &workingState, value, "WorkingState")

    member me.IsWorkstateIn(state) = not(me.Started) || (not(me.WorkingState.Equals(state)) && me.IsOverTime)
