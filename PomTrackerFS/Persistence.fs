﻿module PomodoroTracker.Persistence

open System

type WorkKind = Work | Break
type TimeSpent = { Kind:WorkKind; Begin:DateTime; End:DateTime}
let totalTime ts = (ts.End - ts.Begin)

/// get total time of TimeSpent list
let totalTime1 k = List.filter (fun t -> t.Kind = k)
                   >> List.fold (fun tsum t -> tsum + (totalTime t)) TimeSpan.Zero

type ErrorInfo = string

type IPomodoroStorage =
    inherit IDisposable

    /// <summary>
    /// Load tracked time of the specific date (time part is ignored)
    /// </summary>
    abstract LoadTrackedTime : System.DateTime -> Choice<TimeSpent list,ErrorInfo>

    /// <summary>
    /// Store new tracked time
    /// </summary>
    abstract NewTrackedTime : TimeSpent -> Choice<unit,ErrorInfo>

[<AutoOpen>]
module Internal =
    type DummyStorage() =
        interface IPomodoroStorage with
            member x.LoadTrackedTime _ = Choice1Of2 []
            member x.NewTrackedTime _ = Choice1Of2 ()

        interface IDisposable with
            member x.Dispose() = ()
